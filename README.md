### MultilabelClassifier

Module that expands the sklearn OneVsRest classifier Class focusing on MultiLabelClassification Functionalities.

Enables to choose which labels to predict after the classifier is fitted. Enables setting of custom thresholds for 
underlying classifiers that implement a ```predict_proba()``` function.

### Usage


#### Train Multilabel Classifier
Training data ```Y``` is indicator matrix with rows corresponding to labels ```["a", "b", "c"]```
```
multilabelclf = MultiLabelClf(LogisticRegression())
multilabelclf.fit(X, Y, label_names = ["a", "b", "c"])
```

#### Predict chosen labels
```
y_pred = multilabelClf.predict(X_pred, labels=["b", "a"])
```


#### Predict chosen labels using custom thresholds for label probability

```
y_pred = multilabelClf.predict(X_pred,
                               labels=["b", "a"],
                               label_thresholds=[0.25, 0.35])
```


Detailed examples and execution time comparison in ```\examples\examples.ipynb```