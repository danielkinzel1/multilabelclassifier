from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import _estimators_has, _threshold_for_binary_predict, _predict_binary
from sklearn.utils.validation import check_is_fitted, _num_samples
from sklearn.utils.metaestimators import available_if

import numpy as np
import scipy.sparse as sp
import array
import warnings


class MultiLabelCLf(OneVsRestClassifier):
    """
    MultiLabelClassifier Object that wraps a scikit-learn OneVsRestClassifier. Designated to Multilabel Classification
    Problems only.
    """

    def __init__(self, estimator, **kwargs):
        super().__init__(estimator, **kwargs)
        self.__labels = None
        self.__label_names = None

    @property
    def labels(self):
        return self.__labels

    @property
    def label_names(self):
        return self.__label_names

    def fit(self, X: np.ndarray, y: np.ndarray, label_names=None) -> None:
        """
        Fit underlying estimator to given Training Data and labels

        :param X: 2d numpy Matrix: Data
        :param y: 2d numpy Matrix: indicating Labels of Data
        :param label_names: Names of the labels
        """
        # Save all originally in training data existing label names
        if label_names is not None:
            assert len(label_names) == y.shape[1], "label_names must match number of labels"
            assert len(set(label_names)) == len(label_names), "label_names must contain unique values"
            for label in label_names:
                assert isinstance(label, str), "label_names must be of type str"
                assert len(label) > 0, "Label must not be empty string"
            self.__label_names = {label_names[i]: i for i in range(len(label_names))}
        else:
            self.__label_names = {i: i for i in range(y.shape[1])}

        super(MultiLabelCLf, self).fit(X, y)
        if hasattr(self, "classes_"):
            self.__labels = self.classes_
        else:
            raise Exception("could not get classes_ from OneVsRestClassifier Object")
        assert self.multilabel_, "Values in y do not represent a multilabel classification task"

    @available_if(_estimators_has("predict_proba"))
    def predict_proba(self, X: np.ndarray, labels: list = None) -> np.ndarray:
        """
        Predict Probabilities for Labels for given Data

        :param X: Data
        :param labels: List of labels to predict.
        Leave blank to use all Labels the classifier was trained with
        :return: 2d one hot encoded Numpy Matrix indicating probability of labels
        """
        check_is_fitted(self)

        # If no labels specified, use all that were given in Training Data
        if labels is None:
            labels = self.labels
        else:
            for label in labels:
                assert label in self.labels, "specified label not in trained labels of classifier"
        Y = np.array([self.estimators_[self.__label_names[e]].predict_proba(X)[:, 1] for e in labels]).T
        return Y

    def predict(self, X, labels: list = None, label_thresholds: list = None) -> np.ndarray:
        """
        Predict labels for given Data

        :param X: Data
        :param labels: list of labels to predict
        :param label_thresholds: custom thresholds for label
        :return: 2d one hot encoded Numpy Matrix indicating labels of Data
        """
        check_is_fitted(self)

        # If no labels specified, use all that were given in Training Data
        if labels is None:
            labels = self.__label_names
        else:
            for label in labels:
                assert label in self.label_names, "specified label not in trained labels of classifier"

        # The following Codeblocks are partially taken from sklearn itself
        n_samples = _num_samples(X)

        # Distinguish if custom threshold for binary classification per label
        if label_thresholds is None:
            use_pred_proba = False
            thresh = [_threshold_for_binary_predict(self.estimators_[0])] * len(labels)
        else:
            use_pred_proba = True
            thresh = []
            assert len(label_thresholds) == len(labels), "len of thresholds must match len of specified labels"
            for t in label_thresholds:
                try:
                    thresh.append(float(t))
                except ValueError:
                    raise ValueError("each threshold must be a real number")

        indices = array.array("i")
        indptr = array.array("i", [0])

        if use_pred_proba and not _estimators_has("pred_proba"):
            warnings.warn("Underlying estimator does not implement pred_proba required for custom label thresholds. "
                          "Using default decision function")

        # Make prediction per label, built sparse output array
        j = 0
        for label in labels:
            e = self.estimators_[self.__label_names[label]]
            if use_pred_proba:
                indices.extend(np.where(e.predict_proba(X)[:, 1] > thresh[j])[0])
            else:
                indices.extend(np.where(_predict_binary(e, X) > thresh[j])[0])
            indptr.append(len(indices))
            j += 1
        data = np.ones(len(indices), dtype=int)
        indicator = sp.csc_matrix((data, indices, indptr), shape=(n_samples, len(labels)))

        # return dense 2d array
        return indicator.toarray()

    def predict_old(self, X):
        return super(MultiLabelCLf, self).predict(X)
