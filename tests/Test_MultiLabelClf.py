from multilabelclf.MultiLabelClf import MultiLabelCLf
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn import svm
import unittest
import numpy as np
import numpy.testing as np_test


class test_MultiLabelClf(unittest.TestCase):
    # Label Matrix indicating 4 possible labels
    y = np.array([[1, 0, 0, 1],
                  [0, 0, 1, 1],
                  [0, 0, 0, 0]])

    # Feature Matrix - just some nonsense values
    x = np.array([[10, 10],
                  [12, 12],
                  [0.5, 7.5]])

    def test_basic_input(self):
        # Test Exception-safe functionality

        # Create random nonsense binary matrix that represent labels
        y_dummy = np.array([np.random.randint(low=0, high=2, size=100, dtype="int8") for i in range(5)]).transpose()
        x_dummy = np.array([np.random.randint(low=0, high=10, size=100, dtype="int8") for i in range(2)]).transpose()

        # Instantiate MultiLabelClassifier, fit and predict - SVM
        multi_clf = MultiLabelCLf(svm.SVC())
        multi_clf.fit(x_dummy, y_dummy)
        y_pred = multi_clf.predict(x_dummy)

        # Instantiate MutliLabelClassifier, fit and predict - LogisticRegression
        multi_clf = MultiLabelCLf(LogisticRegression())
        multi_clf.fit(x_dummy, y_dummy)
        y_pred = multi_clf.predict(x_dummy)

    def test_thresholds(self):
        # Test for thresholds

        # Create random nonsense binary matrix that represent labels
        y_dummy = np.array([np.random.randint(low=0, high=2, size=100, dtype="int8") for i in range(5)]).transpose()
        x_dummy = np.array([np.random.randint(low=0, high=10, size=100, dtype="int8") for i in range(2)]).transpose()
        label_names = ["A", "B", "C", "D", "E"]

        # Instantiate MutliLabelClassifier, fit and predict - LogisticRegression
        multi_clf = MultiLabelCLf(LogisticRegression())
        multi_clf.fit(x_dummy, y_dummy, label_names=label_names)
        y_pred = multi_clf.predict(x_dummy, labels=["C", "A"], label_thresholds=[0,1])
        self.assertEqual(y_pred[:, 0].sum(), 100)
        self.assertEqual(y_pred[:, 1].sum(), 0)

    def test_equal_outcomes(self):
        # Test equivalent outcome to sklearn OneVsRest classifier

        # Create random binary matrix that represent labels
        y_dummy = np.array([np.random.randint(low=0, high=2, size=100, dtype="int8") for i in range(5)]).transpose()
        x_dummy = np.array([np.random.randint(low=0, high=10, size=100, dtype="int8") for i in range(2)]).transpose()

        # Test equal outcomes
        my_clf = MultiLabelCLf(estimator=LogisticRegression())
        my_clf.fit(x_dummy, y=y_dummy, label_names=["A", "B", "C", "D", "E"])
        y_pred_old = my_clf.predict_old(x_dummy)
        y_pred = my_clf.predict(x_dummy)
        np_test.assert_array_equal(y_pred, y_pred_old)

    def test_malicious_input(self):
        # Define some Test Data
        y_dummy = np.array([1, 2, 3])
        y_dummy_correct = np.array([[1, 0, 1],
                                    [0, 0, 1],
                                    [1, 1, 0]])
        x_dummy = np.array([[1, 2, 3],
                            [4, 5, 6],
                            [7, 8, 9]])

        my_clf = MultiLabelCLf(estimator=LogisticRegression())

        # Test we cannot accidentally create a multiclass Classifier
        with self.assertRaises(Exception):
            my_clf.fit(X=x_dummy, y=y_dummy, label_names=["a", "b", "c"])

        # Test we cannot hand over less thresholds than specified
        my_clf.fit(X=x_dummy, y=y_dummy_correct)
        with self.assertRaises(AssertionError):
            my_clf.predict(X=x_dummy, label_thresholds=[0.1, 0.4])

        # Test we cannot hand over nonsense threshold values
        with self.assertRaises(ValueError):
            my_clf.predict(x_dummy, label_thresholds=["a", "b", "c"])
